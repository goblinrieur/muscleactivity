# MuscleActivity

Mesure d'activité musculaire (précisément mesure de l'effort) sous LICENSE [MIT](./LICENSE), à réutiliser librement dans les limites de la license.

[3 x électrodes]->[Ampli diff]->[filtre]->[beep/sound]

Baser ça sur une batterie simple comme 2 piles/blocs 9V ou alors un Lipo 3S à 11v, régulé à 3 tensions (_+9v +5v & -5v_).

# code du synoptique 

```
digraph S {                          
        label="synoptique simple"
        node [margin=0 fontcolor=blue fontsize=10 fillcolor="burlywood" style=filled]
        bgcolor="beige"
        A [label="electrodes n°1", shape=diamond]
        B [label="electrodes n°2", shape=diamond]
        C [label="electrodes n°3", shape=diamond]      
        D [label="Amplification differentielle"]
        E [label="Filtre", shape=rectangle]
        F [label="amplificateur tension", shape=rectangle]
        G [label="Reglage du gain", shape=octagon, fillcolor="white"]
        H [label="Reglage volume sonore", shape=octagon, fillcolor="white"]
        I [label="Amplification audio", shape=rectangle]
        J [label="Mini-haut Parleur", shape=trapezium]
        { rank = same; F ; G ; }
        { rank = same; H ; I ; }
        A -> D
        B -> D
        C -> D -> E -> F -> G -> F                       
        F -> I -> H -> I -> J
}                   
```

![synoptique](./synoptic.jpeg)

# mesure

Ce que l'on mesure ici est l'effort et uniquement l'effort, rien d'autre. (_pas la force, pas l'énergie consommée, ni rien d'autre_).

Les électrodes sont des électrodes universelles à contact cutané, (_trouvable en pharmacie également_), selon le muscle visé, généralement sur le bras, l'avant bras, l'arrière du mollet, et le coté de la cuisse, la précision est plus ou moins fiable. Aussi on conseille d'utiliser sur ces 4 zones. L'idéal est de poser/coller les électrodes à quelques centimètres les unes des autres, alignées sur la courbure du muscle.

Un faible effort, sera détecté sous la forme de vagues de fréquences sur temps, avec des alternances, lors d'un effort un peu plus soutenu, on aura une suite de vagues de fréquences légèrement incrémentées, et sur un gros effort, on verra une courbe quasi linéaire en fréquence/temps (_limite une moyenne sous une forme proche d'un ax+b_).

Le tout sous la forme d'une \courbe{f}{t}. C'est ce signal qui est filtré, puis amplifié et converti en signal sonore simple dans la gamme de fréquences audibles.

# schema 

J'utilise ici KICAD 6.x pour la réalisation du projet. Les fichiers gerbers pour la production du PCB ainsi que les fichiers de perçages sont fourni dans le projet (_dans le répertoire correspondant_).

Les choix de composants sont dépendants de mon stock de composants, mais vous pouvez facilement remplacer ça à votre guise, en vérifiant les brochages et éventuellement en ajustant les valeurs des composants passifs.

![schema](./schema.png)

[schema zoom](./schema_v0.0.0.pdf)

[répertoire de travail KICAD](./MMA_DIY)

# PCB

Le choix est simplement défini par les dimensions du boîtier que je vais utiliser. Les composants étant très courants et avec peu de particularités, il sera _relativement_ facile de modifier si nécessaire. 

Ce choix d'adaptation au boîtier permet aussi de simplifier le routage du PCB par un usage massif de connecteurs pour les composants _utilisateur_ tels que les potentiomètres, interrupteurs, haut-parleurs etc.

![dessous](./MMA_DIY_bot.png)

![dessus](./MMA_DIY_fac.png)

![dimensions](./MMA_DIY_dim.png)

[PDF du PCB](./PCB_p.pdf)
